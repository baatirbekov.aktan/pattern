$(() => {
    $(document).on('click', '#sidebarToggle', function () {
        document.body.classList.toggle('sb-sidenav-toggled');
    })

    $(document).on('click', '.sort', function (e) {
        sort(e);
    })

    $(document).on('click', '.clean-input', function (e) {
        const name = $(this).data('name');
        $(`input[name=${name}]`).val('');

        filter();
    });

    $(document).on('click', '.clean-select', function (e) {
        const name = $(this).data('name');
        $(`select[name=${name}]`).val('');

        filter();
    });

    $(document).on('input', '*[form="search"]', function (e) {
        filter();
    });

    $(document).on('click', '.page-item a', function (e) {
        e.preventDefault();
        paginate(e);
    });
})


function filter() {
    const form = $('#search');
    const table = form.data('table');
    const data = form.serialize();

    $.ajax({
        method: 'GET',
        url: `/admin/${table}`,
        data: data
    }).done(successResponse => {
        $(`#${table}-table`).replaceWith(successResponse.table)
        window.history.replaceState({}, table, successResponse.url);
    })
}

function paginate(e) {
    const element = e.currentTarget;
    const url = $(element).attr('href')
    const form = $('#search');
    const table = form.data('table');

    $.ajax({
        method: 'GET',
        url: url,
        cache: false
    }).done(successResponse => {
        $(`#${table}-table`).replaceWith(successResponse.table)
        window.history.replaceState({}, table, successResponse.url);
    })
}

function sort(e) {
    const element = e.currentTarget;
    let type = $(element).data('order-by') ? $(element).data('order-by') : 'DESC';
    const name = $(element).data('name');
    const form = $('#search');
    const table = form.data('table');
    const data = form.serializeArray();

    if (type === 'ASC') {
        type = 'DESC';
    } else {
        type = 'ASC';
    }

    data.push(
        {name: "order_by", value: name},
        {name: "order_type", value: type}
    );

    $.ajax({
        method: 'GET',
        url: `/admin/${table}`,
        data: $.param(data)
    }).done(successResponse => {
        $(`#${table}-table`).replaceWith(successResponse.table)
        window.history.replaceState({}, table, successResponse.url);
    })
}
