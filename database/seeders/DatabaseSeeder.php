<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        $this->call(RoleSeeder::class);

        $adminRole = Role::where('name', 'admin')->first();

        \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'email' => 'admin@admin.com',
        ])->roles()->attach($adminRole->id);
    }
}
