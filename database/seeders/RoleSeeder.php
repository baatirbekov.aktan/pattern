<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::factory()->state(['name' => 'admin', 'description' => 'Администратор сайта'])->create();
        Role::factory()->state(['name' => 'moderator', 'description' => 'Модератор сайта'])->create();
    }
}
