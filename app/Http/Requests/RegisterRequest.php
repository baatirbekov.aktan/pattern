<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:2', 'string', 'regex:/([^(<)(>)])$/u',],
            'email' => ['required', 'email:rfc', 'unique:users,email', 'regex:/([^(<)(>)])$/u',],
            'password' => ['required', 'string', 'min:6', 'confirmed', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
