<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View|JsonResponse
     */
    public function index(Request $request): View|Factory|JsonResponse|Application
    {
        $table = User::query()->getQuery()->from;

        $users = User::filter()
            ->order()
            ->paginate(Session::get("{$table}.limit") ?? 8)
            ->withQueryString();

        $roles = Role::all();

        Session::put("{$table}.url", $request->fullUrl());

        if ($request->ajax()) {
            return response()->json(
                [
                    'table' => view('admin.users.table', [
                        'users' => $users,
                        'roles' => $roles,
                        'table' => $table
                    ])->render(),
                    'url' => $request->fullUrl()
                ],
                200
            );
        }

        return view('admin.users.index', [
            'users' => $users,
            'roles' => $roles,
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(User $user): RedirectResponse
    {
        $this->authorize('delete', $user);
        $user->delete();
        return redirect()->back()->with('success', 'Successfully deleted');
    }
}
