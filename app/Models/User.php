<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Searchable;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    /**
//     * Get the indexable data array for the model.
//     *
//     * @return array
//     */
//    public function toSearchableArray(): array
//    {
//        return $this->toArray();
//    }

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * @param $role
     * @return mixed
     */
    public function hasRole($role): mixed
    {
        return $this->roles()->where('name', $role)->first();
    }

    /**
     * @param Builder $query
     * @return void
     */
    public function scopeFilter(Builder $query): void
    {
        $query->when(request('search'), function (Builder $q) {
            $q->where('name', 'LIKE', '%' . request('search') . '%')
                ->orWhere('email', 'LIKE', '%' . request('search') . '%');
        });

    }

    /**
     * @param Builder $query
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function scopeOrder(Builder $query): void
    {
        request()->whenFilled('order_type', function () {
            Session::put("{$this->table}.order_type", request('order_type'));
        });

        request()->whenFilled('order_by', function () {
            Session::put("{$this->table}.order_by", request('order_by'));
        });

        if (request()->filled('limit') && Session::get("{$this->table}.limit") != request()->get('limit')) {
            Session::put("{$this->table}.limit", request('limit'));
        }

        $query->orderBy(
            Session::get("{$this->table}.order_by") ?? 'id',
            Session::get("{$this->table}.order_type") ?? 'DESC'
        );
    }
}
