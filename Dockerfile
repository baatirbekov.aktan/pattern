FROM php:8.1-fpm

# Copy composer.json
COPY composer.json /var/www/

# Copy package.json
COPY package.json /var/www/

# Set working directory
WORKDIR /var/www

# Install extensions
RUN apt-get update && apt-get install -y \
  libzip-dev \
  libonig-dev \
  libpq-dev \
  build-essential \
  libpng-dev \
  libmagickwand-dev \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libfreetype6-dev \
  locales \
  zip \
  jpegoptim optipng pngquant gifsicle \
  vim \
  unzip \
  git \
  curl \
 && docker-php-ext-configure gd --with-freetype --with-jpeg \
 && docker-php-ext-configure pgsql --with-pgsql=/usr/local/pgsql \
 && docker-php-ext-install pdo_pgsql pgsql mbstring zip exif pcntl gd

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Node.js
RUN curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install nodejs -y
RUN npm install -g npm@8.19.2

# Add user
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
