<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->name('admin.')->middleware(['role:admin'])->group(function () {
    Route::resources([
        'users' => \App\Http\Controllers\Admin\UserController::class,
    ]);

    Route::get('/', function () {
        return view('admin.dashboard');
    })->name('dashboard');
});
